const num = Math.round(Number(prompt("Please enter a number")));
console.log("The number you provided is " + num + ".");

for (let i = num; i > 0; i--) {
  if (i <= 50) {
    console.log('The current value is at 50. Terminating the loop.');
    break;
  }

  if (i % 10 === 0) {
    console.log("The number is divisible by 10. Skipping the number.");
  } else if (i % 5 === 0) {
    console.log(i);
  }
}

const word = 'supercalifragilisticexpialidocious';
let consonants = '';

for (let i = 0; i < word.length; i++) {
  const character = word[i];
  if (character === 'a' ||
      character === 'e' ||
      character === 'i' ||
      character === 'o' ||
      character === 'u') {
    continue
  } else {
    consonants += character;
  }
}

console.log(word);
console.log(consonants);
